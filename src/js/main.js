import React from "react";
import ReactDOM from "react-dom/client";

function Root () {
    switch (window.endpoint) {
        case "blog.view": {
            return <h1>hello</h1>;
        }
    }
}

ReactDOM.createRoot(document.querySelector(".react-root"))
        .render(<Root />)