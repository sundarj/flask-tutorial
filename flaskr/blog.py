from flask import (
    Blueprint,
    flash,
    g,
    redirect,
    render_template,
    request,
    url_for
)

from werkzeug.exceptions import abort

from flaskr.auth import login_required
from flaskr.db import get_db

bp = Blueprint("blog", __name__)

@bp.route("/")
def index():
    """Render index template with queried posts."""
    db = get_db()
    posts = db.execute(
        "SELECT p.id, title, body, created, author_id, username, liked"
        " FROM post p JOIN user u ON p.author_id = u.id"
        " ORDER BY created DESC"
    ).fetchall()
    return render_template("blog/index.html", posts=posts)

@bp.route("/create", methods=("GET", "POST"))
@login_required
def create():
    """Render create template, with login required. If submitting the form,
flash an error when the title is blank, otherwise insert post into the
database and redirect to the index."""
    if request.method == "POST":
        title = request.form["title"]
        body = request.form["body"]
        error = None

        if not title:
            error = "Title is required."

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                "INSERT INTO post (title, body, author_id)"
                " VALUES (?, ?, ?)",
                (title, body, g.user["id"])
            )
            db.commit()
            return redirect(url_for("blog.index"))

    return render_template("blog/create.html")

def get_post(id, check_author=True):
    """Try to fetch a post with the given id. If no such post exists, abort
with a 404. If check_author is True, and the logged in user is not the author
of the post, abort with a 403."""
    post = get_db().execute(
        "SELECT p.id, title, body, created, author_id, username, liked"
        " FROM post p JOIN user u ON p.author_id = u.id"
        " WHERE p.id = ?",
        (id,)
    ).fetchone()

    if post is None:
        abort(404, f"Post id {id} doesn't exist.")

    if check_author and post["author_id"] != g.user["id"]:
        abort(403)

    return post

@bp.route("/<int:id>/update", methods=("GET", "POST"))
@login_required
def update(id):
    """Login required. Render update template, with post acquired with get_post
with the given id. If submitting the form, render an error when the title is
missing, otherwise update the post's title and body and redirect to the index.
"""
    post = get_post(id)

    if request.method == "POST":
        title = request.form["title"]
        body = request.form["body"]
        error = None

        if not title:
            error = "Title is required."

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                "UPDATE post SET title = ?, body = ?"
                " WHERE id = ?",
                (title, body, id)
            )
            db.commit()
            return redirect(url_for("blog.index"))

    return render_template("blog/update.html", post=post)

@bp.route("/<int:id>/delete", methods=("POST",))
@login_required
def delete(id):
    """Login required. get_post the post with the given id, then delete it
then redirect to index."""
    get_post(id)
    db = get_db()
    db.execute("DELETE FROM post WHERE id = ?", (id,))
    db.commit()
    return redirect(url_for("blog.index"))


@bp.route("/<int:id>")
def view(id):
    """get_post the post with the given id, and query for any comments, then
render the view template with them."""
    post = get_post(id, check_author=False)
    comments = get_db().execute(
        "SELECT c.id, body, created, author_id, username"
        " FROM comment c JOIN user u ON c.author_id = u.id"
        " WHERE post_id = ?",
        (id,)
    ).fetchall()
    return render_template("blog/view.html", post=post, comments=comments)

@bp.route("/<int:id>/like", methods=("POST",))
def like(id):
    """Get liked status from request and update the post's liked column to
match. Redirect to index."""
    liked = int(request.form["liked-" + str(id)])
    db = get_db()
    db.execute(
        "UPDATE post SET liked = ?"
        " WHERE id = ?",
        (1 if liked == 0 else 0, id)
    )
    db.commit()
    return redirect(url_for("blog.index"))

@bp.route("/<int:id>/comment", methods=("POST",))
@login_required
def comment(id):
    """Get comment body from request. If empty, show an error. Otherwise,
insert comment into database and redirect to view route."""
    body = request.form["body"]
    error = None

    if not body:
        error = "A body is required."

    if error is not None:
        flash(error)
    else:
        db = get_db()
        db.execute(
                "INSERT INTO comment (body, post_id, author_id)"
                " VALUES (?, ?, ?)",
                (body, id, g.user["id"])
            )
        db.commit()
    return redirect(url_for("blog.view", id=id))
